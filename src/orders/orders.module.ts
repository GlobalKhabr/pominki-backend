import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Orders, OrdersDishes, Statuses } from './orders.entity';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { Customers } from '../customers/customers.entity';
import { CustomersService } from '../customers/customers.service';
import { DishesService } from '../dishes/dishes.service';
import { Dishes } from '../dishes/dishes.entity';
import { Categories } from '../categories/categories.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Orders,
      OrdersDishes,
      Statuses,
      Customers,
      Dishes,
      Categories,
    ]),
  ],
  providers: [OrdersService, CustomersService, DishesService],
  controllers: [OrdersController],
})
export class OrdersModule {}
