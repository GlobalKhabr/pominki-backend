import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Customers } from '../customers/customers.entity';
import { Dishes } from '../dishes/dishes.entity';

@Entity('orders')
export class Orders {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Customers, (customer) => customer.id)
  customer: number;

  @ManyToOne(() => Statuses, (status) => status.id)
  status: string;

  @ManyToMany(() => OrdersDishes)
  @JoinTable()
  dishes: Dishes[];

  @Column({ type: 'datetime' })
  createdAt: Date;
}

@Entity('statuses')
export class Statuses {
  @PrimaryColumn()
  @OneToMany(() => Orders, (order) => order.status)
  id: string;

  @Column({ nullable: false })
  name: string;
}

@Entity('orders_dishes')
export class OrdersDishes {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Dishes, (dish) => dish.id)
  dish: number | Dishes;

  @ManyToOne(() => Orders, (order) => order.id)
  order: number;
}
