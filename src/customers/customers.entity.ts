import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('customers')
export class Customers {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  phone: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column({ type: 'datetime' })
  createdAt: Date;
}
