import {
  Controller,
  Get,
  Post,
  UseGuards,
  Request,
  UseInterceptors,
} from '@nestjs/common';
import { AppService } from './app.service';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { UsersService } from './users/users.service';
import { TransformInterceptor } from './transform.interceptor';
import {
  ApiBearerAuth,
  ApiBody,
  ApiExcludeController,
  ApiExcludeEndpoint,
  ApiOperation,
  ApiProperty,
} from '@nestjs/swagger';
import { LoginDto } from './auth/dto/login.dto';

@Controller()
export class AppController {
  constructor(
    private appService: AppService,
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @ApiExcludeEndpoint()
  @Get()
  getIndex(): object {
    return this.appService.getIndex();
  }

  @ApiOperation({ summary: 'Авторизация и поулчение токена' })
  @ApiBody({ type: LoginDto })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @ApiOperation({ summary: 'Получение данных пользователя' })
  @ApiBearerAuth()
  @UseInterceptors(TransformInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return this.usersService.findOne({ id: req.user.id });
  }
}
