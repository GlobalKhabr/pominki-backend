import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) {}

  async findOne(data): Promise<Users> {
    return this.usersRepository.findOne(data, {
      select: ['id', 'email', 'role'],
    });
  }
  async findOneForAuth(data): Promise<Users> {
    return this.usersRepository.findOne(data);
  }
}
