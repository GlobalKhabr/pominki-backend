import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();

    if (status === 200 || status === 201) {
      return response.status(status).json({
        status,
        data: response,
      });
    }
    const message = exception['response']['message'];
    return response.status(status).json({
      status,
      message,
    });
  }
}
