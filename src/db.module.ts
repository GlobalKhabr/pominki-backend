import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '31.31.203.10',
      port: 3306,
      username: 'wetop',
      password: 'QP[6GeCIiTIe2Vh0',
      database: 'pominki',
      autoLoadEntities: true,
      // entities: ['src/**/**.entity{.ts,.js}'],
      synchronize: true,
    }),
  ],
})
export class DbModule {}
