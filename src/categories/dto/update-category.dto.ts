import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateCategoryDto {
  @ApiProperty()
  public name: string;

  @IsNotEmpty()
  public updatedAt = new Date();
}
