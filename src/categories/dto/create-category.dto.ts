import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCategoryDto {
  @ApiProperty()
  @IsNotEmpty()
  public name: string;

  @IsNotEmpty()
  public createdAt = new Date();

  @IsNotEmpty()
  public updatedAt = new Date();
}
