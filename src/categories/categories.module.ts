import { Module } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Categories } from './categories.entity';
import { Dishes } from '../dishes/dishes.entity';
import { OrdersDishes } from '../orders/orders.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Categories, Dishes, OrdersDishes])],
  providers: [CategoriesService],
  controllers: [CategoriesController],
})
export class CategoriesModule {}
