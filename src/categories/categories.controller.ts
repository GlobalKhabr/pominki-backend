import {
  Controller,
  Get,
  Param,
  Post,
  Delete,
  Body,
  NotFoundException,
  UseInterceptors,
  Put,
  UploadedFile,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CategoriesService } from './categories.service';
import { TransformInterceptor } from '../transform.interceptor';
import { InsertResult } from 'typeorm';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { FileUploadDto } from '../dto/file.upload.dto';

@ApiTags('categories')
@Controller('categories')
@UseInterceptors(TransformInterceptor)
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @ApiOperation({ summary: 'Получение всех категорий' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get()
  async getAll() {
    return await this.categoriesService.getAll();
  }

  @ApiOperation({ summary: 'Получение категории по ID' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getOne(@Param('id') id: number) {
    const model = await this.categoriesService.getById(id);
    if (model) {
      return model;
    } else {
      throw new NotFoundException('Category not found');
    }
  }

  @ApiOperation({ summary: 'Создание новой категории' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() data: CreateCategoryDto) {
    const model: InsertResult = await this.categoriesService.create(data);
    return model.raw.insertId;
  }

  @ApiOperation({ summary: 'Загрузка изображения по ID категории' })
  @ApiParam({ name: 'id', required: true })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './uploads/categories',
        filename(req, file, cb) {
          cb(null, `${req.params.id}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  @Post(':id/upload')
  async uploadFile(@Param('id') id, @UploadedFile() file: Express.Multer.File) {
    if (!file) throw new BadRequestException('No image file');
    if (await this.categoriesService.getById(id)) {
      return this.categoriesService.handleUpload(
        id,
        '/' + file.path.replace(/\\+/g, '/'),
      );
    } else {
      throw new NotFoundException('Category not found');
    }
  }

  @ApiOperation({ summary: 'Редактирование категории по ID' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(@Param('id') id: number, @Body() data: UpdateCategoryDto) {
    const model = await this.categoriesService.getById(id);
    if (model) {
      await this.categoriesService.update(id, data);
    } else {
      throw new NotFoundException('Category not found');
    }
  }

  @ApiOperation({ summary: 'Удаление категории по ID' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: number) {
    const model = await this.categoriesService.getById(id);
    if (model) {
      await this.categoriesService.remove(id);
    } else {
      throw new NotFoundException('Category not found');
    }
  }
}
