export class UpdateDishDto {
  readonly name: string;
  readonly image: string;
  readonly price: number;
  readonly weight: number;
  readonly size: number;
  readonly updatedAt = new Date();
}
