import { IsNotEmpty } from 'class-validator';

export class CreateDishDto {
  @IsNotEmpty()
  public name: string;

  @IsNotEmpty()
  public category: number;

  public image: string;

  @IsNotEmpty()
  public price = 0;
  @IsNotEmpty()
  public weight = 0;
  @IsNotEmpty()
  public size = 0;

  @IsNotEmpty()
  public createdAt = new Date();
  @IsNotEmpty()
  public updatedAt = new Date();
}
