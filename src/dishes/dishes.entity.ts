import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Categories } from '../categories/categories.entity';

@Entity('dishes')
export class Dishes {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Categories, (category) => category.id)
  category: number;

  @Column({ length: 50, nullable: false })
  name: string;

  @Column({ length: 50, nullable: true })
  image: string;

  @Column({ default: 0, type: 'double' })
  price: number;

  @Column({ default: 0, type: 'double' })
  weight: number;

  @Column({ default: 0, type: 'double' })
  size: number;

  @Column({ type: 'datetime' })
  createdAt: Date;

  @Column({ type: 'datetime' })
  updatedAt: Date;
}
