import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dishes } from './dishes.entity';
import { CreateDishDto } from './dto/create-dish.dto';
import { UpdateDishDto } from './dto/update-dish.dto';
import { Categories } from '../categories/categories.entity';
import { OrdersDishes } from '../orders/orders.entity';

@Injectable()
export class DishesService {
  constructor(
    @InjectRepository(Dishes)
    private dishesRepository: Repository<Dishes>,
    @InjectRepository(Categories)
    public categoriesRepository: Repository<Categories>,
    @InjectRepository(OrdersDishes)
    public ordersDishesRepository: Repository<OrdersDishes>,
  ) {}

  /**
   * Конструктурирование блюд
   */
  find() {
    return this.dishesRepository
      .createQueryBuilder('dishes')
      .leftJoinAndSelect(
        'dishes.category',
        'categories',
        'dishes.categoryId = categories.id',
      )
      .select([
        'dishes.id',
        'dishes.name',
        'dishes.image',
        'dishes.price',
        'dishes.weight',
        'dishes.size',
        'categories.id',
        'categories.name',
        'categories.image',
      ]);
  }

  /**
   * Получить все блюда
   */
  getAll(): Promise<Dishes[]> {
    return this.find().getMany();
  }

  /**
   * Получить все блюда по категории
   * @param id
   */
  async getAllByCategory(id: number): Promise<Dishes[]> {
    if (!id) throw new BadRequestException('Category is not specified');
    const category = await this.categoriesRepository.findOne({ id });
    if (category) {
      return this.find().where({ category: id }).getMany();
    } else {
      throw new NotFoundException('Category not found');
    }
  }

  /**
   * Получить блюдо по ID
   * @param id
   */
  getById(id: number): Promise<Dishes> {
    return this.find().where('dishes.id = :id', { id }).getOne();
  }

  /**
   * Создание нового блюда
   * @param data
   */
  async create(data: CreateDishDto) {
    return this.dishesRepository.insert(data);
  }

  /**
   * Удаление блюда по ID
   * @param id
   */
  async remove(id) {
    await this.ordersDishesRepository.delete({ dish: id });
    return this.dishesRepository.delete(id);
  }

  /**
   * Обновление блюда по ID
   * @param id
   * @param data
   */
  async update(id: number, data: UpdateDishDto) {
    return this.dishesRepository.update({ id }, data);
  }

  /**
   * Обработка фотографии
   * @param id
   * @param file
   */
  async handleUpload(id: number, file) {
    await this.dishesRepository.update({ id }, { image: file });
    return;
  }
}
