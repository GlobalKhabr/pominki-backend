import { Module } from '@nestjs/common';
import { DishesController } from './dishes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dishes } from './dishes.entity';
import { DishesService } from './dishes.service';
import { Categories } from '../categories/categories.entity';
import { CategoriesService } from '../categories/categories.service';
import { CategoriesModule } from '../categories/categories.module';
import { OrdersDishes } from '../orders/orders.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Dishes, Categories, OrdersDishes]),
    CategoriesModule,
  ],
  providers: [DishesService],
  controllers: [DishesController],
})
export class DishesModule {}
