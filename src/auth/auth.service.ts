import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcryptjs';
import { LoginDto } from './dto/login.dto';
@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}
  async validateUser(email: string, password: string) {
    const user = await this.userService.findOneForAuth({ email });

    if (!user) throw new UnauthorizedException();

    const tmp = await bcrypt.compare(password, user.password);
    if (user && tmp) {
      return user;
    }
    return false;
  }
  async login(user: LoginDto) {
    const payload = { id: user.id, email: user.email, loggedIn: new Date() };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
