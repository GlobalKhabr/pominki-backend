import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  public id: number;

  public email: string;

  @ApiProperty()
  public username: string;

  @ApiProperty()
  public password: string;
}
